#!/bin/bash
modelname=""
param1=$1

show_model()
{
cat << EOF
以下为脚本管理所有的模块，输入对应模块前面的数字
1 qws_gcserver
2 qws_appserver2
3 qws_pushserver
4 qws_smsserver
5 qws_wechat_device
EOF
}

update()
{
cd /Users/fengmeng/working/go/qws_server/
tyrepo sync
cd /Users/fengmeng/working/go/qws_golang/ext_libs
svn update
cd /Users/fengmeng/working/go/qws_golang/int_libs
svn update
}

compile()
{
cd  /Users/fengmeng/working/go/qws_server/src/$modelname
echo `pwd`
if [  -f $modelname ]; then
   rm -f modelname
fi
GOOS=linux GOARCH=amd64 go build
}

process()
{
    if [ $param1 = "update" ]
    then
        update
        compile
    else
        echo "skip update"
    fi
}

qws_gcserver()
{
    /usr/bin/expect << EOF
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "sudo mv -f /home/360qws/qws_gcserver/qws_gcserver /home/360qws/qws_gcserver/qws_gcserver.backup\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "exit\r"
    expect
    set                 timeout 5
    spawn scp  -r -P 22  /Users/fengmeng/working/go/qws_server/src/qws_gcserver/qws_gcserver fengmeng@192.168.0.135:/home/360qws/qws_gcserver/
    expect              "*assword:*"
    send                "密码\r"
    expect
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_gcserver/&&chmod 755 qws_gcserver && sudo chown 360qws:360qws qws_gcserver\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect
    send                "pkill -f  qws_gcserver\r"
    expect
    send              "nohup ./qws_gcserver >err.log & \r"
    expect
    puts             "qws_gcserver start success"
EOF
}

qws_appserver2()
{
    /usr/bin/expect << EOF
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "sudo mv -f /home/360qws/qws_appserver2/qws_appserver2 /home/360qws/qws_appserver2/qws_appserver2.backup\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "exit\r"
    expect
    set                 timeout 5
    spawn scp  -r -P 22  /Users/fengmeng/working/go/qws_server/src/qws_appserver2/qws_appserver2 fengmeng@192.168.0.135:/home/360qws/qws_appserver2/
    expect              "*assword:*"
    send                "密码\r"
    expect
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_appserver2/&&chmod 755 qws_appserver2 && sudo chown 360qws:360qws qws_appserver2\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect
    send                "pkill -f  qws_appserver2\r"
    expect
    send              "nohup ./qws_appserver2 >err.log & \r"
    expect
    puts             "qws_appserver2 start success"
EOF
}

qws_pushserver()
{
    /usr/bin/expect << EOF
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "sudo mv -f /home/360qws/qws_pushserver/qws_pushserver /home/360qws/qws_pushserver/qws_pushserver.backup\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "exit\r"
    expect
    set                 timeout 5
    spawn scp  -r -P 22  /Users/fengmeng/working/go/qws_server/src/qws_pushserver/qws_pushserver fengmeng@192.168.0.135:/home/360qws/qws_pushserver/
    expect              "*assword:*"
    send                "密码\r"
    expect
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_pushserver/&&chmod 755 qws_pushserver && sudo chown 360qws:360qws qws_pushserver\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect
    send                "pkill -f  qws_pushserver\r"
    expect
    send              "nohup ./qws_pushserver >err.log & \r"
    expect
    puts             "qws_pushserver start success"
EOF
}

qws_smsserver()
{
    /usr/bin/expect << EOF
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "sudo mv -f /home/360qws/qws_smsserver/qws_smsserver /home/360qws/qws_smsserver/qws_smsserver.backup\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "exit\r"
    expect
    set                 timeout 5
    spawn scp  -r -P 22  /Users/fengmeng/working/go/qws_server/src/qws_smsserver/qws_smsserver fengmeng@192.168.0.135:/home/360qws/qws_smsserver/
    expect              "*assword:*"
    send                "密码\r"
    expect
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_smsserver/&&chmod 755 qws_smsserver && sudo chown 360qws:360qws qws_smsserver\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect
    send                "pkill -f  qws_smsserver\r"
    expect
    send              "nohup ./qws_smsserver >err.log & \r"
    expect
    puts             "qws_smsserver start success"
EOF
}

qws_wechat_device()
{
    /usr/bin/expect << EOF
    set                 timeout 2
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "sudo mv -f /home/360qws/qws_wechat_device/qws_wechat_device /home/360qws/qws_wechat_device/qws_wechat_device.backup\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "exit\r"
    expect
    spawn scp  -r -P 22  /Users/fengmeng/working/go/qws_server/src/qws_wechat_device/qws_wechat_device fengmeng@192.168.0.135:/home/360qws/qws_wechat_device/
    expect              "*assword:*"
    send                "密码\r"
    expect
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_wechat_device/&&chmod 755 qws_wechat_device && sudo chown 360qws:360qws qws_wechat_device\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect
    send                "pkill -f  qws_wechat_device\r"
    expect
    send              "nohup ./qws_wechat_device >err.log & \r"
    expect
    puts             "qws_wechat_device start success"
EOF
}

show_model
read -r -p "选择一个需要部署的模块:" response
case "$response" in
    1)
        modelname="qws_gcserver"
        process
        qws_gcserver
        ;;
    2)
        modelname="qws_appserver2"
        process
        qws_appserver2
        ;;
    3)
        modelname="qws_pushserver"
        process
        qws_pushserver
        ;;
    4)
        modelname="qws_smsserver"
        process
        qws_smsserver
        ;;
    5)
        modelname="qws_wechat_device"
        process
        qws_wechat_device
        ;;
    *)
        echo "模块不存在，退出"
        ;;
esac
