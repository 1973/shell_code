#!/bin/bash

show_model()
{
cat << EOF
以下为脚本管理所有的模块，输入对应模块前面的数字进行重启
1 qws_gcserver
2 qws_appserver2
3 qws_pushserver
4 qws_smsserver
5 qws_wechat_device
EOF
}

qws_gcserver()
{
    /usr/bin/expect << EOF
    set              timeout 1
    spawn            ssh -p  22  fengmeng@192.168.0.135
    expect           "*assword:*"
    send             "密码\r"
    expect
    send             "sudo su 360qws\r"
    expect           "*assword:*"
    send             "密码\r"
    expect
    send             "cd /home/360qws/qws_gcserver/&&pkill -f  qws_gcserver\r"
    expect
    send             "nohup ./qws_gcserver >err.log & \r"
    expect
    send             "\n\r"
    expect
    send             "tail -n 50 *.log\r"
    expect
    puts             "qws_gcserver reboot tart success"
EOF
}

qws_appserver2()
{
    /usr/bin/expect << EOF
    set                 timeout 1
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_appserver2/&&pkill -f  qws_appserver2\r"
    expect
    send              "nohup ./qws_appserver2 >err.log & \r"
    expect
    send             "\n\r"
    expect
    send              "tail -n 50 *.log\r"
    expect
    puts              "qws_appserver2 reboot tart success"
EOF
}

qws_pushserver()
{
    /usr/bin/expect << EOF
    set                 timeout 1
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_appserver2/&&pkill -f  qws_appserver2\r"
    expect
    send              "nohup ./qws_appserver2 >err.log & \r"
    expect
    send             "\n\r"
    expect
    send              "tail -n 50 *.log\r"
    expect
    puts              "qws_appserver2 reboot tart success"
EOF
}

qws_smsserver()
{
    /usr/bin/expect << EOF
    set                 timeout 1
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_smsserver/&&pkill -f  qws_smsserver\r"
    expect
    send              "nohup ./qws_smsserver >err.log & \r"
    expect
    send             "\n\r"
    expect
    send              "tail -n 50 *.log\r"
    expect
    puts              "qws_smsserver reboot tart success"
EOF
}

qws_wechat_device()
{
    /usr/bin/expect << EOF
    set                 timeout 1
    spawn               ssh -p  22  fengmeng@192.168.0.135
    expect              "*assword:*"
    send                "密码\r"
    expect
    send               "sudo su 360qws\r"
    expect              "*assword:*"
    send                "密码\r"
    expect
    send                "cd /home/360qws/qws_wechat_device/&&pkill -f  qws_wechat_device\r"
    expect
    send              "nohup ./qws_wechat_device >err.log & \r"
    expect
    send             "\n\r"
    expect
    send              "tail -n 50 *.log\r"
    expect
    puts              "qws_wechat_device reboot tart success"
EOF
}

show_model
read -r -p "选择一个需要重新启动的模块:" response
case "$response" in
    1)
        modelname="qws_gcserver"
        qws_gcserver
        ;;
    2)
        modelname="qws_appserver2"
        qws_appserver2
        ;;
    3)
        modelname="qws_pushserver"
        qws_pushserver
        ;;
    4)
        modelname="qws_smsserver"
        qws_smsserver
        ;;
    5)
        modelname="qws_wechat_device"
        qws_wechat_device
        ;;
    *)
        echo "模块不存在，退出"
        ;;
esac
